<?php

namespace Vitrin\Infrastructure\Core\Projection;

use ReflectionClass;
use Vitrin\Infrastructure\Types\DateRange;
use Spatie\EventSourcing\Projections\Projection;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\App;

abstract class CoreProjection extends Projection
{
    public function isWriteable(): bool
    {
        return App::isLocal() ?: $this->isWriteable;
    }

    public function newCollection(array $models = [])
    {
        collect((new ReflectionClass($this))->getTraitNames())
            ->map(fn ($trait) => 'newCollection' . last(explode("\\", $trait)))
            ->each(function ($trait) use (&$models) {
                method_exists($this, $trait) && ($models = $this->$trait($models));
            });

        return new Collection($models);
    }

    /**
     * Scope a query for a given date range.
     */
    public function scopeWhereDateRange(Builder $query, string $field, DateRange $range): void
    {
        $range->hasStart() && $query->where($field, '>=', $range->from());
        $range->hasEnd() && $query->where($field, '<=', $range->to());
    }

    public function scopeWhereRelationField(Builder $query, string $field, int|array $id): void
    {
        is_array($id)
            ? $query->whereIn($field, $id)
            : $query->where($field, $id);
    }

    public function getKeyName()
    {
        return 'id';
    }

    public function getKeyType()
    {
        return 'integer';
    }

    public function getIncrementing()
    {
        return true;
    }

    public function getRouteKeyName()
    {
        return 'id';
    }
}

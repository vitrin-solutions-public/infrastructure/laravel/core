<?php

namespace Vitrin\Infrastructure\Core\Model;

use Illuminate\Support\Str;

/**
 * Trait HasUUID
 *
 * Automatically assigns a UUID to the model's 'uuid' attribute upon creation.
 * Ensures 'uuid' is unique and set only if it's not already present.
 *
 * @package Vitrin\Infrastructure\Core\Model
 * @author Amir Khadangi <amirkhadangi920@gmail.com>
 */
trait HasUUID
{
    /**
     * Get the name of the UUID attribute.
     *
     * @return string
     */
    public static function getUUIDKey()
    {
        return 'uuid';
    }

    /**
     * Boot the trait by hooking into the model's creating event.
     * Assigns a UUID to the model's UUID attribute if it's not already set.
     */
    protected static function bootHasUUID()
    {
        static::creating(
            fn ($model) =>
                empty($model->uuid) && ($model->{self::getUUIDKey()} = Str::uuid())
        );
    }
}

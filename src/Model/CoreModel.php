<?php

namespace Vitrin\Infrastructure\Core\Model;

use EloquentFilter\Filterable;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use Vitrin\Infrastructure\Contracts\Model\ModelContract;
use Vitrin\Infrastructure\Types\DateRange;

abstract class CoreModel extends Model implements ModelContract
{
    use Filterable;

    /**
     * Scope a query for a given date range.
     */
    public function scopeWhereDateRange(Builder $query, string $field, DateRange $range): void
    {
        $range->hasStart() && $query->where($field, '>=', $range->from());
        $range->hasEnd() && $query->where($field, '<=', $range->to());
    }

    public function scopeWhereRelationField(Builder $query, string $field, int|array $id): void
    {
        is_array($id)
            ? $query->whereIn($field, $id)
            : $query->where($field, $id);
    }
    
    public function newCollection(array $models = [])
    {
        collect((new ReflectionClass($this))->getTraitNames())
            ->map(fn($trait) => 'newCollection' . last(explode("\\", $trait)))
            ->each(function($trait) use(&$models) {
                method_exists($this, $trait) && ($models = $this->$trait($models));
            });

        return new Collection($models);
    }
}

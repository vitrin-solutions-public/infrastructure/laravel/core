<?php

namespace Vitrin\Infrastructure\Core\Aggregate;

use Illuminate\Support\Facades\App;
use Ramsey\Uuid\Uuid;
use Spatie\EventSourcing\AggregateRoots\AggregateRoot;

class CoreAggregate extends AggregateRoot
{
    public static function newInstance(): static
    {
        return self::retrieve(Uuid::uuid4());
    }

    // public static function retrieve(string $uuid): static
    // {
    //     $aggregateRoot = App::make(static::class);

    //     $aggregateRoot->uuid = $uuid;

    //     $aggregateRoot->aggregateVersion = 3;
    //     $aggregateRoot->aggregateVersionAfterReconstitution = $aggregateRoot->aggregateVersion;

    //     return $aggregateRoot;

    //     return $aggregateRoot->reconstituteFromEvents();
    // }
}

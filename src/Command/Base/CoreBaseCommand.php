<?php

namespace Vitrin\Infrastructure\Core\Command\Base;

use Illuminate\Support\Facades\App;
use Spatie\LaravelData\Data;

class CoreBaseCommand extends Data
{
    public function handle()
    {
        $handler = str(get_class($this))->replaceLast('Command', 'Handler')->value();

        return App::make($handler)->handle($this);
    }
}

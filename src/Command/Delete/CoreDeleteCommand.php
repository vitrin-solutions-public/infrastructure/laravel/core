<?php

namespace Vitrin\Infrastructure\Core\Command\Delete;

use Vitrin\Infrastructure\Contracts\Command\Delete\DeleteCommandContract;
use Vitrin\Infrastructure\Core\Command\Base\CoreBaseCommand;

abstract class CoreDeleteCommand extends CoreBaseCommand implements DeleteCommandContract
{
    //
}
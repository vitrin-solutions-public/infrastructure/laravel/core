<?php

namespace Vitrin\Infrastructure\Core\Command\Delete;

trait CoreDeleteCommandTrait
{
    public function getIdentifier(): int|string
    {
        return $this->id;
    }
}

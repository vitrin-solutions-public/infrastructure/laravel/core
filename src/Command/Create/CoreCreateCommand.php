<?php

namespace Vitrin\Infrastructure\Core\Command\Create;

use Vitrin\Infrastructure\Contracts\Command\Create\CreateCommandContract;
use Vitrin\Infrastructure\Core\Command\Base\CoreBaseCommand;

abstract class CoreCreateCommand extends CoreBaseCommand implements CreateCommandContract
{
    public ?string $uuid;

    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }
}

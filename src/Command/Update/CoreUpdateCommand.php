<?php

namespace Vitrin\Infrastructure\Core\Command\Update;

use Vitrin\Infrastructure\Contracts\Command\Update\UpdateCommandContract;
use Vitrin\Infrastructure\Core\Command\Base\CoreBaseCommand;

abstract class CoreUpdateCommand extends CoreBaseCommand implements UpdateCommandContract
{

}
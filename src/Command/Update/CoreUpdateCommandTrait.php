<?php

namespace Vitrin\Infrastructure\Core\Command\Update;

trait CoreUpdateCommandTrait
{
    public function setIdentifier(int|string $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getIdentifier(): int|string
    {
        return $this->id;
    }
}

<?php

namespace Vitrin\Infrastructure\Core\Facade;

use Illuminate\Support\Facades\Facade;
use Vitrin\Infrastructure\Contracts\Facade\FacadeContract;

abstract class CoreFacade extends Facade implements FacadeContract
{

}
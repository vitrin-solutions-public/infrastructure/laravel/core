<?php

namespace Vitrin\Infrastructure\Core\Query\Find;

trait CoreFindQueryTrait
{
    public function getIdentifier(): int
    {
        return $this->id;
    }
}

<?php

namespace Vitrin\Infrastructure\Core\Query\Find;

use Vitrin\Infrastructure\Contracts\Query\Find\FindQueryContract;
use Vitrin\Infrastructure\Core\Query\Base\CoreBaseQuery;

abstract class CoreFindQuery extends CoreBaseQuery implements FindQueryContract
{
    //
}
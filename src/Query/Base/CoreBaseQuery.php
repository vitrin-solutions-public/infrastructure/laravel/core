<?php

namespace Vitrin\Infrastructure\Core\Query\Base;

use Illuminate\Support\Facades\App;
use Spatie\LaravelData\Data;

abstract class CoreBaseQuery extends Data
{
    public function handle()
    {
        $handler = str(get_class($this))->replaceLast('Query', 'Handler')->value();

        return App::make($handler)->handle($this);
    }
}

<?php

namespace Vitrin\Infrastructure\Core\Query\List;

use Spatie\LaravelData\Attributes\Validation\Max;
use Spatie\LaravelData\Attributes\Validation\Min;
use Vitrin\Infrastructure\Contracts\Query\List\ListQueryContract;
use Vitrin\Infrastructure\Core\Query\Base\CoreBaseQuery;

abstract class CoreListQuery extends CoreBaseQuery implements ListQueryContract
{
    #[Min(1)]
    public int $page = 1;

    #[Min(1), Max(1000)]
    public int $per_page = 20;

    public array $sort = [];

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPerPage(): int
    {
        return min($this->per_page, 1000);
    }
}

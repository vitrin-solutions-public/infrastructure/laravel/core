<?php

namespace Vitrin\Infrastructure\Core\ValueObject;

use Spatie\LaravelData\Data;
use Vitrin\Infrastructure\Contracts\ValueObject\ValueObjectContract;

abstract class CoreValueObject extends Data implements ValueObjectContract
{
    //
}

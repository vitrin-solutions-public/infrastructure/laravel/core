<?php

namespace Vitrin\Infrastructure\Core\Repository;

/**
 * @mixin CoreRepository
 */
trait HasProjection
{
    protected bool $is_writable = false;

    public function writeable(): static
    {
        $this->is_writable = true;

        return $this;
    }

    public function unwriteable(): static
    {
        $this->is_writable = false;

        return $this;
    }

    public function isWritable(): bool
    {
        return $this->is_writable;
    }
}

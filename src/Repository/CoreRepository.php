<?php

namespace Vitrin\Infrastructure\Core\Repository;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use ReflectionClass;
use Spatie\LaravelData\DataCollection;
use Vitrin\Infrastructure\Contracts\Command\Create\CreateCommandContract;
use Vitrin\Infrastructure\Contracts\Command\Update\UpdateCommandContract;
use Vitrin\Infrastructure\Contracts\Entity\EntityContract;
use Vitrin\Infrastructure\Contracts\Query\Find\FindQueryContract;
use Vitrin\Infrastructure\Contracts\Query\List\ListQueryContract;
use Vitrin\Infrastructure\Contracts\Repository\RepositoryContract;
use Vitrin\Infrastructure\Core\Entity\CoreEntity;

/**
 * @template T
 * @extends Hooks<T>
 * @use Hooks
 */
abstract class CoreRepository implements RepositoryContract
{
    use Hooks;

    protected $model;

    protected $entity;

    protected $filter;

    public function __construct()
    {
        throw new Exception('please override the constructor method in repository class and assign $model & $filter');
    }

    /**
     * create a new instance of repositorie's model with the given data array
     *
     * @param array $data
     * @return Model|CoreEntity
     */
    public function get_new_instance(array $data): Model|CoreEntity
    {
        return new (get_class($this->model))($data);
    }

    public function withRelations(): array
    {
        return [];
    }

    public function hasProjection(): bool
    {
        return in_array(
            'Vitrin\Infrastructure\Contracts\Repository\ProjectionContract',
            (new ReflectionClass($this))->getInterfaceNames()
        );
    }

    public function isWritable(): bool
    {
        return false;
    }

    /**
     * find a model base on it's id and return the eloquent model
     *
     * @param int $id
     * @return EntityContract|T
     */
    public function find(FindQueryContract $query): ?EntityContract
    {
        empty(array_filter(array_values($query->toArray()), fn ($i) => $i))
            && throw new Exception($query::class . " returns empty array for find !");

        $model = $this->model
            ->filter($query->toArray(), $this->filter)
            ->with($this->withRelations())
            ->first();

        return $model ? $this->entity::from($model) : null;
    }

    /**
     * find a model base on it's id and return the eloquent model
     *
     * @param FindQueryContract $query
     * @return EntityContract|T
     */
    public function findOrFail(FindQueryContract $query): EntityContract
    {
        return $this->find($query) ?: App::abort(404, "Data for " . class_basename($query) . " with identifier: " . $query->getIdentifier() . " not found !");
    }

    /**
     * find a lot of model base in their id's and return a collection of them
     *
     * @param array $ids
     * @return DataCollection|EntityContract|T[]
     */
    public function list(ListQueryContract $query): DataCollection
    {
        return $this->entity::collect(
            $this->model
                ->filter($query->toArray(), $this->filter)
                ->with($this->withRelations())
                ->get()
        );
    }

    /**
     * Get {$per_page} times of models of {$page} argument
     *
     * @param ListQueryContract $query
     * @return LengthAwarePaginator
     */
    public function paginated(ListQueryContract $query): LengthAwarePaginator
    {
        return $this->entity::collect(
            $this->model
                ->filter($query->toArray(), $this->filter)
                ->with($this->withRelations())
                ->latest()
                ->paginate($query->getPerPage(), ['*'], 'page', $query->getPage())
        );
    }

    /**
     * clone a model and fill attribiutes of cloned model with the given data array
     * finally return the cloned model
     *
     * @param Model $model
     * @param array $data
     * @return T
     */
    public function clone($model, array $data)
    {
        $cloned = $model->replicate();
        $cloned->fill($data);
        $cloned->save();

        return $cloned;
    }

    /**
     * create a data base on passed data argument and return it's model
     *
     * @param CreateCommandContract $command
     * @return EntityContract
     */
    public function create(CreateCommandContract $command): EntityContract
    {
        $model = $this->hasProjection() && $this->isWritable()
            ? $this->model->writeable()->create($command->toArray())
            : $this->model->create($command->toArray());

        $model->load($this->withRelations());

        return $this->entity::from($model);
    }

    /**
     * create a lot of data record base on passed data argument
     * and return true if they created successfully
     *
     * @param array[] $items
     * @return bool
     */
    public function createMany(array $data)
    {
        return $this->model->insert($data);
    }

    /**
     * find the model base on it's identifier and update it base on passes data argument
     * if the model updated successfully return true, otherwise return false
     *
     * @param UpdateCommandContract|T $command
     * @return EntityContract|T $model
     */
    public function update(UpdateCommandContract $command): EntityContract
    {
        $model = match(true) {
            is_string($command->getIdentifier())    => $this->model->where('uuid', $command->getIdentifier())->firstOrFail(),
            is_int($command->getIdentifier())       => $this->model->findOrFail($command->getIdentifier()),
        };

        $this->hasProjection() && $this->isWritable()
            ? $model->writeable()->update($command->toArray())
            : $model->update($command->toArray());

        $model->load($this->withRelations());

        return $this->entity::from($model);
    }

    /**
     * Increment a column's value by a given amount.
     *
     * @param Model $model
     * @param  string  $column
     * @param  float|int  $amount
     * @return int
     */
    public function increment($model, string $column, float $amount = 1)
    {
        return $model->increment($column, $amount);
    }

    /**
     * Decrement a column's value by a given amount.
     *
     * @param Model $model
     * @param  string  $column
     * @param  float|int  $amount
     * @return int
     */
    public function decrement($model, string $column, float $amount = 1)
    {
        return $model->decrement($column, $amount);
    }

    /**
     * Increment the given column's values by the given amounts.
     *
     * @param Model $model
     * @param  array<string, float|int|numeric-string>  $columns
     * @return int
     *
     * @throws \InvalidArgumentException
     */
    public function incrementEach($model, array $values)
    {
        return $model->incrementEach($values);
    }

    /**
     * Decrement the given column's values by the given amounts.
     *
     * @param Model $model
     * @param  array<string, float|int|numeric-string>  $columns
     * @return int
     *
     * @throws \InvalidArgumentException
     */
    public function decrementEach($model, array $values)
    {
        return $model->decrementEach($values);
    }

    /**
     * find a lot of models base on their identifiers and update them based on the passed data argument
     * finally return the count of affected models
     *
     * @param integer[] $id
     * @param array $data
     * @return int
     */
    public function updateMany(array $ids, array $data): int
    {
        return $this->model->whereIn('id', $ids)->update($data);
    }

    /**
     * find the model base on it's identifier and delete it from data storage
     * finally return true, if the model removed successfully
     *
     * @param integer $id
     * @return bool
     */
    public function delete(int|string $id): bool
    {
        return (bool) match(true) {
            is_string($id)  => $this->model->where('uuid', $id)->delete(),
            is_int($id)     => $this->model->where('id', $id)->delete(),
        };
    }

    /**
     * find some of models base on their identifiers and delete them from data storage
     * finally return the count if removed items
     *
     * @param integer[] $ids
     * @return int
     */
    public function deleteMany(array $ids): int
    {
        return $this->model->whereIn('id', $ids)->delete();
    }
}

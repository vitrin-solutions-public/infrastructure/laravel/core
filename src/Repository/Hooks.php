<?php

namespace Vitrin\Infrastructure\Core\Repository;

/**
 * @mixin CoreRepository
 */
trait Hooks
{
    /**
     * If you want to filter data before showing them to user
     * you can use this hook
     *
     * @param Builder $query
     * @return void
     */
    public function apply_filters($query)
    {
        //
    }

    /**
     * On some of models need to avoid deleting some models
     * with this hook you can ignore them from delete request
     *
     * @param Builder $query
     * @return void
     */
    public function apply_destory_filter($query)
    {
        //
    }
}

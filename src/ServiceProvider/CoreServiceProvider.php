<?php

namespace Vitrin\Infrastructure\Core\ServiceProvider;

use Illuminate\Support\ServiceProvider;
use Vitrin\Infrastructure\Contracts\ServiceProvider\ServiceProviderContract;

abstract class CoreServiceProvider extends ServiceProvider implements ServiceProviderContract
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
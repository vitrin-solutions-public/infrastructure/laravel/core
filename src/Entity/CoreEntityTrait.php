<?php

namespace Vitrin\Infrastructure\Core\Entity;

use Vitrin\Infrastructure\Types\ID;

trait CoreEntityTrait
{
    // public function store(): Model
    // {
    //     return $this->get_repository()->create($this);
    // }

    // public function sync(): Model
    // {
    //     return $this->get_repository()->update($this);
    // }

    // /**
    //  * @return T
    //  */
    // public function clone(array $data = []): Model
    // {
    //     return $this->get_repository()->clone($this, $data);
    // }

    // public function remove(): bool
    // {
    //     return $this->get_repository()->delete($this->id);
    // }
    
    public function getIdentifier(): ID
    {
        return $this->id;
    }
}

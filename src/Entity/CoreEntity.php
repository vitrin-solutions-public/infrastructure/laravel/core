<?php

namespace Vitrin\Infrastructure\Core\Entity;

use Spatie\LaravelData\Data;
use Vitrin\Infrastructure\Contracts\Entity\EntityContract;

abstract class CoreEntity extends Data implements EntityContract
{
    //
}

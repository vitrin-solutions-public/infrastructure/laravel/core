<?php

namespace Vitrin\Infrastructure\Core\Handler\Event;

use Spatie\EventSourcing\EventHandlers\Projectors\EventQuery;

abstract class CoreEventHandler extends EventQuery
{
    //
}

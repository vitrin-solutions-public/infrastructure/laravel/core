<?php

namespace Vitrin\Infrastructure\Core\Filter;

use EloquentFilter\ModelFilter;
use Exception;
use Illuminate\Support\Facades\Validator;
use Vitrin\Infrastructure\Contracts\Filter\FilterContract;

abstract class CoreFilter extends ModelFilter implements FilterContract
{
    public function sort($sort)
    {
        foreach ($sort as $field => $ordering) {
            !in_array($ordering, ['ASC', 'DESC']) && throw new Exception('Only ASC and DESC are available as sorting type !');

            $methodName = str($field)->prepend('sort on')->studly()->value();

            method_exists($this, $methodName)
                ? $this->{$methodName}($ordering)
                : match (true) {
                    default         => $this->orderBy($field, $ordering)
                };
        }
    }

    public function id($id)
    {
        match (true) {
            is_numeric($id)     => $this->where('id', $id),
            is_string($id)      => $this->where('uuid', $id),
            default             => throw new Exception('id is invalid, only integer and uuid is valid !')
        };
    }

    public function uuid($uuid)
    {
        $this->where('uuid', $uuid);
    }
}

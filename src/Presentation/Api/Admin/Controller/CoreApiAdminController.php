<?php

namespace Vitrin\Infrastructure\Core\Presentation\Api\Admin\Controller;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Vitrin\Infrastructure\Contracts\Service\CrudServiceContract;

/**
 * @template T
 */
abstract class CoreApiAdminController
{
    protected CrudServiceContract $service;

    // abstract public function __construct(CrudServiceContract $service);

    protected function service()
    {
        !isset($this->service) && throw new Exception('🙃 You must override __construct(CrudServiceContract $service) in ' . class_basename(static::class));

        return $this->service;
    }

    public function index()
    {
        return Response::json([
            'data' => App::call([$this->service(), 'list'])
        ]);
    }

    public function show()
    {
        return Response::json([
            'data' => App::call([$this->service(), 'find'])
        ]);
    }

    public function store()
    {
        return Response::json([
            'data' => App::call([$this->service(), 'create'])
        ], 201);
    }

    public function update()
    {
        return Response::json([
            'data' => App::call([$this->service(), 'update'])
        ]);
    }

    public function destroy()
    {
        return Response::json([
            'data' => App::call([$this->service(), 'delete'])
        ]);
    }

    // public function destroy_many(Request $request)
    // {
    //     $request->validate([
    //         'ids'       => 'required|array|min:1',
    //         'ids.*'     => 'required|integer',
    //     ]);
    //     $ids = $request->get('ids');

    //     $this->check_permission_destroy_many($ids);

    //     $this->before_destroy($ids);

    //     $result = $this->service->delete_many($ids);

    //     $this->after_destroy($ids, $result);
    // }

    /**
     * kill the request proccess and return the error message with appropriate status code as a response
     *
     * @param integer $status
     * @param string $message
     * @return void
     */
    public function error_response(int $status, string $message)
    {
        App::abort($status, $message);
    }

    public function message_response(string $message)
    {
        return Response::json([
            'message' => $message
        ]);
    }

    // /**
    //  * get the single item and make the response for it
    //  * if the controller has the resource class, return item base on resource, otherwise return simple json response
    //  *
    //  * @param mixed $item
    //  */
    // public function item_response($item)
    // {
    //     return $this->resouce
    //         ? new $this->resouce($item)
    //         : Response::json([
    //             'data' => $item
    //         ]);
    // }

    // /**
    //  * get the collection or array of items and make the response for it
    //  * if the controller has the collection class, return items base on collection, otherwise return simple json response
    //  *
    //  * @param Collection|array $items
    //  */
    // public function collection_response($items)
    // {
    //     return $this->collection
    //         ? new $this->collection($items)
    //         : Response::json([
    //             'data' => $items
    //         ]);
    // }
}

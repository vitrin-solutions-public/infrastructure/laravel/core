<?php

namespace App\Infrastructure\Core\Interface\Api\Admin\Resource;

use Illuminate\Http\Resources\Json\JsonResource;

abstract class CoreApiAdminResource extends JsonResource
{
    //
}

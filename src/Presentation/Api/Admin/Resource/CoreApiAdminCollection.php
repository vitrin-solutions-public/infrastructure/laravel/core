<?php

namespace App\Infrastructure\Core\Interface\Api\Admin\Resource;

use Illuminate\Http\Resources\Json\ResourceCollection;

abstract class CoreApiAdminCollection extends ResourceCollection
{
    //
}

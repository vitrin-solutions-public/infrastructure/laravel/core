<?php

namespace Vitrin\Infrastructure\Core\Service;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Vitrin\Infrastructure\Contracts\Entity\EntityContract;
use Vitrin\Infrastructure\Contracts\Service\CrudServiceContract;
use Vitrin\Infrastructure\Types\ID;

/**
 * @template T
 * @use Hooks<T>
 */
abstract class CoreCrudService extends CoreService implements CrudServiceContract
{
    use Hooks;

    /**
     * return the paginated result base on the given criteria
     *
     * @param $pagination
     * @return LengthAwarePaginator
     */
    public function list($pagination)
    {
        return $this->repository->paginated($pagination);
    }

    /**
     * get the data transfer object and parse the array from it for creating or updating models
     *
     * @param $dto
     * @return array
     */
    public function parse_dto($dto): array
    {
        return $dto->toArray();
    }

    /**
     * get the data transfer object and parse the array from it for creating models
     *
     * @param $dto
     * @return array
     */
    public function parse_dto_for_create($dto): array
    {
        return $this->parse_dto($dto);
    }

    /**
     * get the data transfer object and parse the array from it for updating models
     *
     * @param $dto
     * @return array
     */
    public function parse_dto_for_update($dto): array
    {
        return $this->parse_dto($dto);
    }

    /**
     * find a model by it's identifier
     *
     * @param ID $id
     * @return T
     */
    public function find(ID $id): EntityContract
    {
        return $this->repository->find($id);
    }

    /**
     * create a new model and save it in storage base on the given data
     *
     * @param T $data
     * @return T
     */
    public function create(EntityContract $data): EntityContract
    {
        $this->before_create($data);

        $model = $this->repository->create(
            $this->repository->get_new_instance($this->parse_dto_for_create($data))
        );

        $this->after_create($data, $model);

        return $model;
    }

    /**
     * find a model by it's identifier and update it base on the given data
     *
     * @param ID $id
     * @param T $data
     * @return T
     */
    public function update(ID $id, EntityContract $data): EntityContract
    {
        $model = $this->repository->find($id);

        $this->before_update($data, $model);

        $model = $this->repository->update($model->fill($data->toArray()));

        $this->before_update($data, $model);

        return $model;
    }

    /**
     * find a model by it's identifier and delete it
     *
     * @param ID $id
     * @return bool
     */
    public function delete(ID $id): bool
    {
        $this->before_delete([$id]);

        $result = $this->repository->delete($id);

        $this->after_delete([$id], $result);

        return $result;
    }

    /**
     * delete many models from storage based on the given ids
     *
     * @param int[] $ids
     * @return int
     */
    public function delete_many($ids)
    {
        $this->before_delete($ids);

        $result =  $this->repository->delete_many($ids);

        $this->after_delete($ids, $result);

        return $result;
    }
}

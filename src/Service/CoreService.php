<?php

namespace Vitrin\Infrastructure\Core\Service;

use Vitrin\Infrastructure\Contracts\Service\ServiceContract;

/**
 * @template T
 * @use Hooks<T>
 */
abstract class CoreService implements ServiceContract
{
    //
}

<?php

namespace Vitrin\Infrastructure\Core\Service;

/**
 * @template T
 * @mixin CoreService
 */
trait Hooks
{
    /**
     * The function that get the model and run before creating the model
     *
     * @param $request
     * @return void
     */
    public function before_create($dto)
    {
        //
    }

    /**
     * The function that get the model and run after the model was created
     *
     * @param $request
     * @param T $model
     * @return void
     */
    public function after_create($dto, $model)
    {
        //
    }

    /**
     * The function that get the model and run after the model was updated
     *
     * @param $request
     * @param T $model
     * @return void
     */
    public function before_update($dto, $model)
    {
        //
    }

    /**
     * The function that get the model and run before updating the model
     *
     * @param $request
     * @param T $model
     * @return void
     */
    public function after_update($dto, $model)
    {
        //
    }

    /**
     * The function that get the model and run after the model was updated
     *
     * @param array $ids
     * @return void
     */
    public function before_delete($ids)
    {
        //
    }

    /**
     * The function that run after destroing the model
     *
     * @param array $ids
     * @param int $result
     * @return void
     */
    public function after_delete($ids, $result)
    {
        //
    }
}
